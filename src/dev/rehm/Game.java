package dev.rehm;

import java.util.HashMap;
import java.util.Scanner;

public class Game {

    private final HashMap<String, String> choices = new HashMap<>();

    public Game(){
        initializeChoices();
    }

    public void initializeChoices(){
        choices.put("Initial choice", "We begin at the trailhead and proceed along the well traveled path. Will the " +
                "fabled Orra really be right on the well traveled path? " +
                "\nHow do we proceed? \nStick to the path \nGo bushwhacking");
        choices.put("Stick to the path","We continue down the path and see a large tree down in our way. How should " +
                "we proceed? \nTurn back \nClimb over the tree \nGo bushwhacking");
        choices.put("Go bushwhacking","The forest is thick and the terrain is difficult. You're beginning to get " +
                "fatigued. How should we proceed? \nTurn back \nPower through it");
        choices.put("Power through it", "After hours of fighting through the forest, you finally see an Orra tree in" +
                " the distance! We did it! Let's grab some of its sap and get out of here. The end.");
        choices.put("Climb over the tree", "As you climb over the tree, you realize your arm brushes a cluster of " +
                "Rashimi mushrooms, an extremely poisonous fungi. You see a rash starting to form and hurry back " +
                " to town to get treatment. The end.");
        choices.put("Turn back","We follow our footsteps back to the trailhead. We did not find the Orra tree but we " +
                "made it back safely. The end.");
    }

    public void play(){
        System.out.println("Welcome to the enchanted forest. Legend has it, this is where we can find the fabled Orra" +
                " tree. This tree's sap is used to create an elixir that makes it so you live forever. We must proceed " +
                "cautiously though, who knows what dangers lurk in the shadows.");
        System.out.println(choices.get("Initial choice"));
        getSelection();
    }

    private void getSelection(){
        try (Scanner sc = new Scanner(System.in)) {
            String selection = sc.nextLine();
            if(selection == null || choices.get(selection)==null){
                System.out.println("Incorrect selection. Please enter a valid selection.");
                getSelection();
            }
            String nextStep = choices.get(selection);
            System.out.println(nextStep);
            if(!nextStep.endsWith("The end.")){
                getSelection();
            }
        }
    }
}
